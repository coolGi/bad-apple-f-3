package com.coolgi.badapple.mixin;

import com.coolgi.badapple.BadApple;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.DebugScreenOverlay;
import net.minecraft.client.player.KeyboardInput;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.List;
import java.util.Scanner;

@Mixin(DebugScreenOverlay.class)
public class MixinDebugScreenOverlay {
    private static Scanner video = new Scanner(BadApple.accessFile(BadApple.video));
    private static String[] lastFrame = new String[BadApple.frameHeight];
    private static int frame = -1;
    private static long startTime = 0;

    @Inject(method = "getSystemInformation", at = @At("RETURN"))
    private void addCustomF3(CallbackInfoReturnable<List<String>> cir) {
        if (startTime == 0)
            startTime = System.currentTimeMillis();

        List<String> messages = cir.getReturnValue();
        messages.clear(); // Removes everything from the right side of the f3

        // Move down a few lines
        messages.add("");
        messages.add("");
        messages.add("");

        int currentFrame = (int) Math.floor((System.currentTimeMillis()-startTime)/BadApple.frameLength);
        if (
                currentFrame > frame
                ) {
            frame = currentFrame;
            lastFrame = new String[BadApple.frameHeight];
            for (int i = 0; i < BadApple.frameHeight; i++)
                if (video.hasNext())
                    lastFrame[i] = video.nextLine();
        }

        // Where the rendering happens
        for (String s: lastFrame)
            messages.add(s);

        if (!video.hasNext()) {
            video = new Scanner(BadApple.accessFile(BadApple.video)); // Use this to reset line
            startTime = System.currentTimeMillis();
        }
    }

    private static void updateClick() {
        KeyboardInput input = new KeyboardInput(Minecraft.getInstance().options);

    }
}
