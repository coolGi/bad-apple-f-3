package com.coolgi.badapple.fabric;

import com.coolgi.badapple.BadApple;
import net.fabricmc.api.ModInitializer;

public class MainFabric implements ModInitializer {
    @Override
    public void onInitialize() {
        BadApple.init();
    }
}
