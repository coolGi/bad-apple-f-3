package com.coolgi.badapple.forge;

import com.coolgi.badapple.ModInfo;
import com.coolgi.badapple.BadApple;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(ModInfo.ID)
public class MainForge {
    public MainForge() {
        // Submit our event bus to let architectury register our content on the right time
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::init);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onClientStart);
    }

    private void init(final FMLCommonSetupEvent event) {
        BadApple.init();
    }

    private void onClientStart(final FMLClientSetupEvent event)
    {

    }
}
